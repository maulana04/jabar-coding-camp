/*soal 1
buatlah variabel-variabel seperti di bawah ini

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
gabungkan variabel-variabel tersebut agar menghasilkan output

saya senang belajar JAVASCRIPT
*/

//jawaban no 1
console.log("soal no 1");
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var res = pertama.substring(0,5);
var ris = pertama.substring(12,19);
var rus = kedua.substring(0,7);
var ros = kedua.substring(7,18);

var ini = (res.concat(ris));
var itu = (rus.concat(ros));
console.log(ini.concat(itu));

console.log("");
//soal 2
/*
uatlah variabel-variabel seperti di bawah ini

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
*catatan :
1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6)
*/

//jawaban no 2

console.log("soal no 2");

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var int_kataPertama=parseInt(kataPertama);
var int_kataKedua = parseInt(kataKedua);
var int_kataKetiga = parseInt(kataKetiga);
var int_kataKeempat = parseInt(kataKeempat);

var hasil = int_kataPertama+(int_kataKedua*int_kataKetiga)+int_kataKeempat;

console.log(hasil);


//soal ke 3
/*
buatlah variabel-variabel seperti di bawah ini

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua; // do your own! 
var kataKetiga; // do your own! 
var kataKeempat; // do your own! 
var kataKelima; // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

Kata Pertama: wah
Kata Kedua: javascript
Kata Ketiga: itu
Kata Keempat: keren
Kata Kelima: sekali
*/

//jawaban no 3
console.log("");
console.log("soal no 3");

var kalimat = 'wah javascript itu keren sekali';
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(18, 24);
var kataKelima = kalimat.substring(25, 31)

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);