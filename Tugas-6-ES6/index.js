// soal no 1
console.log("Soal no 1 ");
console.log("Panjang persegi : 4 ");
console.log("Lebar Persegi : 2 ");
console.log();
const luarPersegi = (luas) => {
    let panjang = 4;
    let lebar = 2;
    luas = panjang * lebar;
    
   return luas;
}
const kelilingPersegi = (keliling) => {
    let panjang = 4;
    let lebar = 2;
    keliling = 2* (panjang + lebar);
    
   return keliling;


}

console.log("luas persegi adalah : ");
console.log(luarPersegi());
console.log();

console.log("keliling persegi panjang adalah : ");
console.log(kelilingPersegi());
console.log();

//soal no 2
//soal

console.log("soal no 2");
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName();

  //soal 3

  // object
  console.log();
  console.log("soal no 3");
    const newObject = {
      firstName: "Muhammad ",
      lastName: "Iqbal Mubarok ",
      address: "Jalan Ranamanyar ",
      hobby: "playing football",
    };
    const {firstName,lastName,address,hobby} = newObject;
    console.log(firstName+lastName+address+hobby);
    
//soal 4
console.log();
console.log("soal no 4");
let array1 = ["Will", "Chris", "Sam", "Holly"]
let array2 = ["Gill", "Brian", "Noel", "Maggie"]
 
// ES6 Way 
 
let combinedArray = [...array1, ...array2]
console.log(combinedArray) 

//soal no 5
console.log();
console.log("soal no 5");
const planet = "earth";
const view = "glass";
const theString = `lorem `+`${view}`+`dolor sit amet`+` consectetur adipiscing elit`+`${planet},`;
console.log(theString);